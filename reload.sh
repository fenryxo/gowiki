#!/bin/bash
set -x
pkill gowiki
if make build-app; then ./build/gowiki & fi

while inotifywait -r -e modify -e move -e create -e delete  .; do
    pkill gowiki
    if make build-app; then ./build/gowiki & fi
done

pkill gowiki
