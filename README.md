![GoWiki logo](static/favicon/favicon-32x32.png) GoWiki
=======================================================

Wiki written in Go. A sample application based on [Writing Web Applications tutorial][1]
and ideas from [Web Development with Go course][2].

Requirements
------------

* Go >= 1.16
* GNU Make
* Podman

Local Set-Up
------------

### PostgreSQL

This project uses Podman to create a pod from [localhost/pod.yml](./localhost/pod.yml)
to run a PostgreSQL server for local development, but you may use Docker or a system
PostgreSQL as well.

If you choose the project default settings, you ma use these commands:

* Run `make run-images` to start a PostgreSQL container.
* Run `make stop-images` to remove a PostgreSQL container.
* Run `make initdb` to initialize database.
* Run `make psql` to connect to the database via psql.

### Application

Specifics

* The application listens on http://localhost:8080
* The application stores page on disk to the `./data` directory.
* Other data are stored into a database.

Utility commands:

* Run `make help-app` to list command-line arguments adn environment variables.
* Run `make build-app` to build the application.
* Run `make run-app` to start the application with automatic reloading when a source file changes.


[1]: https://golang.org/doc/articles/wiki/
[2]: https://www.usegolang.com/
