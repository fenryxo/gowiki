module gitlab.com/fenryxo/gowiki

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/jackc/pgconn v1.8.1
	github.com/jackc/pgx/v4 v4.11.0
	github.com/microcosm-cc/bluemonday v1.0.4
	github.com/russross/blackfriday v1.6.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
