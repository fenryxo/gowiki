package forms

import (
	"strings"

	"gitlab.com/fenryxo/gowiki/pkg/validators"
)

// StringValidator validates a `StringField` value `v` and returns an `error`,
// typically a `*ValidationError`, when the validation fails.
type StringValidator func(v string) error

// StringField is a form field for string values.
type StringField struct {
	Name        string
	Type        string
	Label       string
	Placeholder string
	Required    bool
	MinLength   int
	MaxLength   int
	Validators  []StringValidator
	Errors      ErrList
	Value       string
	RawValue    string
}

// Validate form data, add validation errors and return the result.
// Typically called with an element of `url.Values`.
func (f *StringField) Validate(data []string) bool {
	if len(data) > 1 {
		f.Errors.Add(TooManyValues, "Too many values were provided.")
		return false
	}

	if len(data) == 1 {
		f.RawValue = data[0]
	}

	v := strings.TrimSpace(f.RawValue)
	if !f.validateLength(v) || !f.validateType(v) || !f.runValidators(v) {
		return false
	}

	f.Value = v

	return true
}

func (f *StringField) validateLength(v string) bool {
	s := len(v)

	if f.Required && s == 0 {
		f.Errors.Add(FieldRequired, "This field is required.")
		return false
	}

	if f.MinLength > 0 && s < f.MinLength {
		f.Errors.Addf(TooShort, "This field is too short. The minimal length is %d.", f.MinLength)
		return false
	}

	if f.MaxLength > f.MinLength && s > f.MaxLength {
		f.Errors.Addf(TooLong, "This field is too long. The maximal length is %d.", f.MaxLength)
		return false
	}

	return true
}

func (f *StringField) validateType(v string) bool {
	if f.Type == EmailInput && !validators.IsEmailValid(v) {
		f.Errors.Add(InvalidEmail, "Please enter a valid e-mail address.")
		return false
	}

	return true
}

func (f *StringField) runValidators(v string) bool {
	for _, val := range f.Validators {
		if err := val(v); err != nil {
			f.Errors.AddErr(err)
			return false
		}
	}

	return true
}
