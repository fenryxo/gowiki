// Package mkd renders Markdown to HTML.
package mkd

import (
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
)

// Render is Markdown-to-HTML rendering function.
type Render func(mkd string) string

// NewRender creates a new Markdown-to-HTML rendering function.
func NewRender() Render {
	fl := blackfriday.HTML_TOC | blackfriday.HTML_FOOTNOTE_RETURN_LINKS
	renderer := blackfriday.HtmlRenderer(fl, "title", "css")
	ext := blackfriday.EXTENSION_FENCED_CODE |
		blackfriday.EXTENSION_TABLES |
		blackfriday.EXTENSION_AUTOLINK |
		blackfriday.EXTENSION_STRIKETHROUGH |
		blackfriday.EXTENSION_SPACE_HEADERS |
		blackfriday.EXTENSION_FOOTNOTES |
		blackfriday.EXTENSION_HEADER_IDS |
		blackfriday.EXTENSION_TITLEBLOCK |
		blackfriday.EXTENSION_AUTO_HEADER_IDS |
		blackfriday.EXTENSION_BACKSLASH_LINE_BREAK |
		blackfriday.EXTENSION_DEFINITION_LISTS
	policy := bluemonday.UGCPolicy()

	return func(mkd string) string {
		return string(policy.SanitizeBytes(blackfriday.Markdown([]byte(mkd), renderer, ext)))
	}
}
