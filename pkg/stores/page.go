package stores

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/fenryxo/gowiki/pkg/models"
)

// Page loads and saves Page model.
type Page interface {
	Load(title string) (*models.Page, error)
	Save(p *models.Page) error
}

// FilePage loads and saves Page model from/on filesystem.
type FilePage struct {
	dataDir   string
	extension string
}

// NewFilePage creates new FilePageStore.
func NewFilePage(dataDir, extension string) *FilePage {
	if dataDir == "" {
		dataDir = "."
	}

	if extension == "" {
		extension = ".txt"
	}

	return &FilePage{dataDir, extension}
}

// Load a page.
func (ps *FilePage) Load(title string) (*models.Page, error) {
	filename := ps.dataDir + "/" + title + ps.extension

	body, err := ioutil.ReadFile(filename) // #nosec
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, fmt.Errorf("page %q does not exits: %w: %v ", title, ErrNotExist, err)
		}

		return nil, fmt.Errorf("error loading page %q: %w: %v ", title, ErrIO, err)
	}

	return &models.Page{Title: title, Body: string(body)}, nil
}

// Save the page.
func (ps *FilePage) Save(p *models.Page) error {
	err := os.MkdirAll(ps.dataDir, 0o750)
	if err == nil {
		filename := ps.dataDir + "/" + p.Title + ps.extension
		err = ioutil.WriteFile(filename, []byte(p.Body), 0o600)
	}

	if err != nil {
		return fmt.Errorf("error saving page %q: %w: %v ", p.Title, ErrIO, err)
	}

	return nil
}
