package stores

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jackc/pgconn"
	"gitlab.com/fenryxo/gowiki/pkg/models"
)

var (
	// ErrDuplicateUsername is returned for a duplicate username.
	ErrDuplicateUsername = errors.New("duplicate username")
	// ErrDuplicateEmail is returned for a duplicate email.
	ErrDuplicateEmail = errors.New("duplicate email")
)

// Users is a store for `models.User`.
type Users interface {
	Save(m *models.User) error
	EmailExists(email string) (bool, error)
	UsernameExists(username string) (bool, error)
}

// UsersDB stores `models.User` in a database. Create with `NewUsersDB`.
type UsersDB struct {
	db *sql.DB
}

// NewUsersDB creates a new `UsersDB` store.
func NewUsersDB(db *sql.DB) *UsersDB {
	return &UsersDB{db: db}
}

// Save user model.
func (u *UsersDB) Save(m *models.User) error {
	var err error

	if m.ID > 0 {
		_, err = u.db.Exec(
			`UPDATE "users" SET "username" = $1, "email" = $2, "password" = $3 WHERE "id" = $4`,
			m.Username, m.Email, m.Password, m.ID,
		)
	} else {
		err = u.db.QueryRow(
			`INSERT INTO "users"("username", "email", "password") VALUES($1, $2, $3) RETURNING "id"`,
			m.Username, m.Email, m.Password,
		).Scan(&m.ID)
	}

	if err == nil {
		return nil
	}

	var pgerr *pgconn.PgError
	if errors.As(err, &pgerr) && pgerr.Code == "23505" {
		switch pgerr.ConstraintName {
		case "users_username_key":
			return fmt.Errorf("%w: %v", ErrDuplicateUsername, err)
		case "users_email_key":
			return fmt.Errorf("%w: %v", ErrDuplicateEmail, err)
		}
	}

	return fmt.Errorf("%w: %v", ErrIO, err)
}

// EmailExists returns true if the email has already been registered.
func (u *UsersDB) EmailExists(email string) (bool, error) {
	var c int

	err := u.db.QueryRow(`SELECT COUNT(1) FROM "users" WHERE "email" = $1 LIMIT 1`, email).Scan(&c)
	if err != nil {
		return false, fmt.Errorf("%w: %v", ErrIO, err)
	}

	return c == 1, nil
}

// UsernameExists returns true if the username has already been taken.
func (u *UsersDB) UsernameExists(username string) (bool, error) {
	var c int

	err := u.db.QueryRow(`SELECT COUNT(1) FROM "users" WHERE "username" = $1 LIMIT 1`, username).Scan(&c)
	if err != nil {
		return false, fmt.Errorf("%w: %v", ErrIO, err)
	}

	return c == 1, nil
}
