package models

import "regexp"

// TitleRegex validates page title.
var TitleRegex = regexp.MustCompile("^([a-zA-Z0-9]+)$")

// Page holds page data.
type Page struct {
	Title string
	Body  string
}

// IsTitleValid returns true if the title is valid.
func IsTitleValid(title string) bool {
	return TitleRegex.MatchString(title)
}
