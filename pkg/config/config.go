package config

import "gitlab.com/fenryxo/gowiki/pkg/logging"

// Default configuration.
const (
	defaultDatabaseURL = "postgresql://gowiki:gowiki@127.0.0.1:9100/gowiki?sslmode=disable"
	defaultDataDir     = "data"
	defaultBind        = "localhost:8080"
	defaultLogLevel    = logging.WarnLevel
)

// Config holds application configuration.
type Config struct {
	Bind        string
	DataDir     string
	DatabaseURL string
	LogLevel    logging.Level
}

// Default returns default configuration.
func Default() *Config {
	return &Config{
		Bind:        defaultBind,
		DataDir:     defaultDataDir,
		DatabaseURL: defaultDatabaseURL,
		LogLevel:    defaultLogLevel,
	}
}
