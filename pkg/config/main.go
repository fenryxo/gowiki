// Package config contains application configuration.
package config

import (
	"errors"
	"fmt"
	"os"
)

// Collect configuration from command-line arguments, environment variables and default values.
// Returns an error derived from ErrArgParse, ErrArgHelp, or ErrInvalidValue on failure.
func Collect(prog string, arguments []string, getenv func(name string) (string, bool)) (*Config, error) {
	c := Default()

	err := CollectEnv(c, getenv)

	if err == nil {
		err = CollectArgs(c, prog, arguments)
	}

	if err != nil {
		if errors.Is(err, ErrArgHelp) {
			fmt.Fprintln(os.Stderr)
			UsageEnv()
		} else if !errors.Is(err, ErrArgParse) {
			fmt.Fprintf(os.Stderr, "%s\n\n", err)
			UsageEnv()
		}

		return nil, err
	}

	return c, nil
}
