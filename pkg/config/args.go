package config

import (
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/fenryxo/gowiki/pkg/logging"
)

var (
	// ErrArgParse is returned when argument parsing fails.
	ErrArgParse = errors.New("argument parsing failed")
	// ErrArgHelp is returned when help was requested.
	ErrArgHelp = errors.New("help requested")
	// ErrInvalidValue is returned on invalid value.
	ErrInvalidValue = errors.New("invalid value")
)

type logLevelValue logging.Level

func (v *logLevelValue) String() string {
	return logging.Level(*v).String()
}

func (v *logLevelValue) Set(value string) error {
	level, err := logging.LevelByName(value)
	if err != nil {
		return logging.ErrInvalidLevel
	}

	*v = logLevelValue(level)

	return nil
}

// CollectArgs and update configuration or return an error derived from ErrArgParse, ErrArgHelp or other.
func CollectArgs(c *Config, prog string, args []string) error {
	var help bool

	f := flag.NewFlagSet(prog, flag.ContinueOnError)
	f.StringVar(&c.Bind, "bind", c.Bind, "Hostname and port to bind.")
	f.StringVar(&c.DataDir, "data-dir", c.DataDir, "Directory to store data.")
	f.StringVar(&c.DatabaseURL, "database", c.DatabaseURL, "Database connection URL.")
	logLevel := logLevelValue(c.LogLevel)
	f.Var(&logLevel, "log-level", fmt.Sprintf("The lowest log level to report. One of %s.", logging.LogLevels()))
	f.BoolVar(&help, "help", false, "Show help and exit.")
	f.BoolVar(&help, "h", false, "Show help and exit.")

	err := f.Parse(args)
	c.LogLevel = logging.Level(logLevel)

	switch {
	case err != nil:
		return fmt.Errorf("%w: %v", ErrArgParse, err)
	case help:
		f.Usage()
		return ErrArgHelp
	case f.NArg() > 0:
		fmt.Fprint(os.Stderr, "Unexpected extra arguments.\n")
		f.Usage()

		return fmt.Errorf("%w: extra arguments", ErrArgParse)
	default:
		return nil
	}
}
