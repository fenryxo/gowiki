package handlers

import (
	"net/http"
)

// Home page.
func Home(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/view/FrontPage", http.StatusFound)
}
