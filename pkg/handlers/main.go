// Package handlers contains controllers of MVC.
package handlers

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	// ErrNotFound is used when page is not found.
	ErrNotFound = errors.New("not found")
	// ErrInternal is used for uncaught panics.
	ErrInternal = errors.New("internal error")
)

func must(err error) {
	if err != nil {
		panic(err)
	}
}

// ErrorMiddleware turns panics into error pages.
func ErrorMiddleware(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			e := recover()
			if err, ok := e.(error); ok {
				if errors.Is(err, ErrNotFound) {
					http.NotFound(w, r)
				} else {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
			} else if e != nil {
				http.Error(w, fmt.Sprint(e), http.StatusInternalServerError)
			}
		}()

		h(w, r)
	}
}
