package handlers

import (
	"errors"
	"net/http"

	"gitlab.com/fenryxo/gowiki/pkg/auth"
	"gitlab.com/fenryxo/gowiki/pkg/forms"
	"gitlab.com/fenryxo/gowiki/pkg/models"
	"gitlab.com/fenryxo/gowiki/pkg/stores"
	"gitlab.com/fenryxo/gowiki/pkg/views"
)

// Users controller. Create with NewUsers function.
type Users struct {
	signUpView *views.View
	users      stores.Users
}

// NewUsers creates a new Users controller or panics on failure.
func NewUsers(l *views.Loader, us stores.Users) *Users {
	return &Users{
		signUpView: l.MustLoad("sign-up"),
		users:      us,
	}
}

// SignUp handles user sign-up and panics on failure.
func (u *Users) SignUp(w http.ResponseWriter, r *http.Request) {
	f := forms.NewSignUp(u.users.UsernameExists, u.users.EmailExists)

	if r.Method == "POST" {
		if err := r.ParseForm(); err != nil {
			f.FormErrors.AddErr(err)
		}

		f.Validate(r.PostForm)

		if f.Valid {
			m := models.User{
				ID:       0,
				Username: f.Username.Value,
				Email:    f.Email.Value,
				Password: auth.HashPassword(f.Password.Value),
			}

			err := u.users.Save(&m)
			if err == nil {
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}

			switch {
			case errors.Is(err, stores.ErrDuplicateEmail):
				f.Email.Errors.AddErr(forms.ErrDuplicateEmail)
			case errors.Is(err, stores.ErrDuplicateUsername):
				f.Username.Errors.AddErr(forms.ErrDuplicateUsername)
			default:
				panic(err)
			}
		}
	}

	must(u.signUpView.RenderResponse(w, map[string]interface{}{
		"Form": f,
	}))
}
