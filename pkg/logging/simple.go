package logging

import (
	"fmt"
	"io"
	"sync"
	"time"
)

// SerializedWriter may be used by multiple writers simultaneously.
type SerializedWriter interface {
	// Begin returns a writer to use until End is called.
	Begin() io.Writer
	// End finishes a writing operation.
	End(out io.Writer)
}

// LockedWriter is SerializedWriter implemented using a mutex.
// Create with NewLockedWriter.
type LockedWriter struct {
	out io.Writer
	mu  sync.Mutex
}

// NewLockedWriter created new locked writer.
func NewLockedWriter(out io.Writer) SerializedWriter {
	return &LockedWriter{out: out}
}

// Begin returns a writer to use until End is called.
func (w *LockedWriter) Begin() io.Writer {
	w.mu.Lock()
	return w.out
}

// End finishes a writing operation.
func (w *LockedWriter) End(_ io.Writer) {
	w.mu.Unlock()
}

// SimpleLogger is a simple implementation of a logger interface.
// You may create it with NewSimpleLogger.
type SimpleLogger struct {
	Level  Level
	Output SerializedWriter
}

// NewSimpleLogger creates a new simple logger instance.
func NewSimpleLogger(out SerializedWriter, level Level) *SimpleLogger {
	return &SimpleLogger{
		Output: out,
		Level:  level,
	}
}

// Debug logs with debug log level. See Log method for details.
func (l *SimpleLogger) Debug(msg string, data ...interface{}) {
	l.log(DebugLevel, msg, data)
}

// Info logs with info log level. See Log method for details.
func (l *SimpleLogger) Info(msg string, data ...interface{}) {
	l.log(InfoLevel, msg, data)
}

// Warn logs with warn log level. See Log method for details.
func (l *SimpleLogger) Warn(msg string, data ...interface{}) {
	l.log(WarnLevel, msg, data)
}

// Error logs with error log level. See Log method for details.
func (l *SimpleLogger) Error(msg string, data ...interface{}) {
	l.log(PanicLevel, msg, data)
}

// Panic logs with panic log level and panics. See Log method for details.
func (l *SimpleLogger) Panic(msg string, data ...interface{}) {
	l.log(PanicLevel, msg, data)
}

// Log a message msg with the given log level. Data should be a sequence of key-value pairs.
func (l *SimpleLogger) Log(level Level, msg string, data ...interface{}) {
	if level < Disabled {
		level = Disabled
	} else if level > PanicLevel {
		level = PanicLevel
	}

	l.log(level, msg, data)
}

func (l *SimpleLogger) log(level Level, msg string, data []interface{}) {
	if level >= l.Level {
		out := l.Output.Begin()
		defer l.Output.End(out)
		fmt.Fprintf(out, "%v %s %s", time.Now().Format(time.RFC3339), level, msg)

		n := len(data)
		for i := 0; i < n-1; i += 2 {
			fmt.Fprintf(out, " %s=%q", data[i], data[i+1])
		}

		if n%2 != 0 {
			fmt.Fprintf(out, " extra=%q", data[n-1])
		}

		fmt.Fprintln(out)
	}

	if level == PanicLevel {
		panic(msg)
	}
}
