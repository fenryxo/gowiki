// Package logging provides logging interface and implementations.
package logging

import (
	"errors"
	"fmt"
)

// Level type is used for logging levels.
type Level int

// Logging levels.
const (
	Disabled Level = iota
	DebugLevel
	InfoLevel
	WarnLevel
	ErrorLevel
	PanicLevel
)

// ErrInvalidLevel is returned for invalid log level string.
var ErrInvalidLevel = errors.New("invalid log level")

// Label returns a log label for this level.
func (l Level) Label() string {
	switch l {
	case DebugLevel:
		return "DEBUG"
	case InfoLevel:
		return "INFO "
	case WarnLevel:
		return "WARN "
	case ErrorLevel:
		return "ERROR"
	case PanicLevel:
		return "PANIC"
	default:
		return ""
	}
}

// String returns the name of a log level.
// You can use LevelByName function to convert the name into a log level.
func (l Level) String() string {
	switch l {
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case WarnLevel:
		return "warn"
	case ErrorLevel:
		return "error"
	case PanicLevel:
		return "panic"
	default:
		return "disabled"
	}
}

// LevelByName looks up log level by name or returns ErrInvalidLevel on failure.
// You can use Level.String method to convert the log level into a name.
func LevelByName(name string) (Level, error) {
	switch name {
	case "disabled":
		return Disabled, nil
	case "debug":
		return DebugLevel, nil
	case "info":
		return InfoLevel, nil
	case "warn":
		return WarnLevel, nil
	case "error":
		return ErrorLevel, nil
	case "panic":
		return PanicLevel, nil
	default:
		return Disabled, fmt.Errorf("%w: %q", ErrInvalidLevel, name)
	}
}

// LogLevels lists all log levels.
func LogLevels() []Level {
	return []Level{
		Disabled,
		DebugLevel,
		InfoLevel,
		WarnLevel,
		ErrorLevel,
		PanicLevel,
	}
}

// Logger interface.
type Logger interface {
	// Debug logs with debug log level. See Log method for details.
	Debug(msg string, data ...interface{})
	// Info logs with info log level. See Log method for details.
	Info(msg string, data ...interface{})
	// Warn logs with warn log level. See Log method for details.
	Warn(msg string, data ...interface{})
	// Error logs with error log level. See Log method for details.
	Error(msg string, data ...interface{})
	// Panic logs with panic log level and panics. See Log method for details.
	Panic(msg string, data ...interface{})
	// Log a message msg with the given log level. Data should be a sequence of key-value pairs.
	Log(level Level, msg string, data ...interface{})
}
